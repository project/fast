README.txt for Fast Login module
--------------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainer

INTRODUCTION
------------

A module that implements a Login feature using the Fast Platform. It provides functionality to
enroll with the Fast server to generate app key and secret. This is done automatically when the
module is first installed. Later, if they should be re-generated, the admin user may do so on
the module's config settings page at admin/config/content/fast_login.

The enroll feature also registers a redirect URL (on the site) with the Fast Server. This URL
will be invoked by Fast Server whenever any login attempt is made using Fast. The Fast server 
calls with several GET variables such as challengeId, identifier, oth which are then used
to verify the login attempt by invoking the verify API call on the Fast server. Upon successful
verification, depending upon whether the user already exists or not on the site, appropriate action
is taken based on the config settings specified.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------

 - Install the fast_login module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.
   
 - The module offers a 'Fast Login' block which can be configured to appear on any region.
   When this is done and configured, the Fast Login icon and the inline Fast login box
   will appear, allowing a visitor to login to the site via Fast. Visit
   https://www.drupal.org/docs/8/core/modules/block/overview for further information.
   
AUTHOR/MAINTAINER
-----------------

 - Venkatraman Lakshminarayanan (v_lak)
