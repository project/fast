(function ($) {
	/**
	 * Function to enroll with Fast App server and refresh key & secret
	 */
	refreshKeySecret = function () {
	  var url = drupalSettings.path.baseUrl + 'fast-login/refresh-key-secret';
	  $('#refresh-error').text('').hide();
	  $('.throbber').show();
	  $.ajax({
	    url: location.protocol + '//' + location.host + url,
	    type: 'POST',
	    dataType: 'json',
	    success: function (options) {
	      if (typeof(options['error']) != 'undefined') {
	    	// display error message
		    $('#refresh-error').text(options['error']).show();
	      }
	      else {
	        // Update key and secret
	        $('#fast-app-key').val(options['key']);
	        $('#fast-app-secret').val(options['secret']);
	      }
	      $('.throbber').hide();
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	      var msg = textStatus + errorThrown;
	      // display error message
	      $('#refresh-error').text(msg).show();
	      $('.throbber').hide();
	    }
	  });
	};

	/**
	 * Function to select custom radio button when clicked on the custom textfield
	 */
	selectCustom = function () {
	  $("input[name=positions]#custom").attr("checked", true);
	}

})(jQuery);
