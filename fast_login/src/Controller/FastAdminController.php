<?php

namespace Drupal\fast_login\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\fast_login\Services\FastService;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Defines FastAdminController class.
 */
class FastAdminController extends ControllerBase {
  private $fastService;

  public function __construct(FastService $x) {
      $this->$fastService = $x;
  }

  public static function create(ContainerInterface $container) {
      $x = $container->get('fast_login.fast_service');
      return new static ($x);
  }

  /**
 * Page callback: Enroll with Fast App server to re-generate app key & secret
 *
 * @see fast_login_menu()
 */
  function refresh_key_secret() {
    // invoke the service and enroll with Fast Service and obtain App key & secret
    $result = $this->$fastService->enroll();
    return new JsonResponse($result);
  }

  function fast_login_goto($path) {
    $response = new RedirectResponse($path);
    //$response->send();
    return $response;
  }

  /**
 * Page callback: Verify login attempt with Fast App server
 *
 * @see fast_login_menu()
 */
  function authenticate() {
    $config = \Drupal::config('fast_login.settings');
    // extract values from the GET vars passed by Fast Service to our redirect URL
    $data = array();
    $data['identifier'] = isset($_GET['identifier']) ? $_GET['identifier'] : '';
    $data['challengeId'] = isset($_GET['challengeId']) ? $_GET['challengeId'] : '';
    $data['oth'] = isset($_GET['oth']) ? $_GET['oth'] : '';
    // fill app key and secret
    $data['key'] = $config->get('fast_app_key');
    $data['secret'] = $config->get('fast_app_secret', '');
    //if (module_exists('devel')) dpm($data);
    // verify login attempt
    $result = $this->$fastService->verify($data);
    if (isset($result['error'])) {
      drupal_set_message($msg, $result['error']);
      return;
    }

    // check if user exists in site database and do redirect appropriately
    $user = user_load_by_mail($data['identifier']);
    if (!empty($user)) {
      //if (module_exists('devel')) dpm($user);
      // user exists - create session
      user_login_finalize($user);
      // now redirect based on config setting
      return($this->fast_login_goto($config->get('fast_user_redirect')));
    }
    else {
      // user does not exist - perform action based on config setting
      $action = $config->get('fast_non_user_action');
      //if (module_exists('devel')) dpm($action);
      $redirect = '/';
      switch ($action) {
        case 'show_error':
          drupal_set_message(
            t('No user with email \'@mail\' found.', array('@mail' => $data['identifier'])),
            'error'
          );
          break;
        case 'create_account':
        case 'create_and_register':
          //set up the user fields
          $fields = array(
            'name' => $data['identifier'],
            'mail' => $data['identifier'],
            'status' => 1,
            'init' => 'email address',
            'roles' => array(
              DRUPAL_AUTHENTICATED_RID => 'authenticated user',
            ),
          );

          //the first parameter is left blank so a new user is created
          $account = entity_create('user', $fields);
          $account->save();
          // create session
          user_login_finalize($account);
          //if (module_exists('devel')) dpm($account);
          $redirect = '/user';
          if ($action == 'create_and_register') {
            $redirect = '/user/register';
          }
          break;
      }
      //if (module_exists('devel')) dpm($redirect);
      return($this->fast_login_goto($redirect));
    }
  }
}