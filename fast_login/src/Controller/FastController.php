<?php

namespace Drupal\fast_login\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines FastController class.
 */
class FastController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Hello, World!'),
    ];
  }

}