<?php

namespace Drupal\fast_login\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Path\AliasManager;
use Drupal\Core\Path\PathValidator;

class FastSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fast_login_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
  	$config = $this->config('fast_login.settings');
  	// app key field.
  	$form['#attached']['library'][] = 'fast_login/fast_login.styles';
    $form['#attached']['library'][] = drupal_get_path('module', 'fast_login').'/js/admin.js';

    $form['app_key'] = array(
      '#type' => 'textfield',
      '#title' => t('App Key:'),
      '#default_value' => $config->get('fast_app_key'),
      '#description' => t('Specify the Fast app key.'),
      '#required' => TRUE,
      '#id' => 'fast-app-key',
      '#size' => 40,
      '#attributes' => array('readonly' => 'readonly')
    );

    $form['app_secret'] = array(
      '#type' => 'textfield',
      '#title' => t('App Secret:'),
      '#default_value' => $config->get('fast_app_secret'),
      '#description' => t('Specify the Fast app secret.'),
      '#required' => TRUE,
      '#id' => 'fast-app-secret',
      '#size' => 40,
      '#attributes' => array('readonly' => 'readonly')
    );

    $html = '';
    $html .= '<div>';
    $html .= '<span>To generate new key and secret, refresh (re-enroll) with Fast App Server </span>';
    $html .= '<input id="refresh-btn" class="button button--primary js-form-submit form-submit" onclick="javascript: return refreshKeySecret();" value="Refresh"></input>';
    $html .= '<div class="ajax-progress-throbber"><div class="throbber">&nbsp;</div></div>';
    $html .= '<div id="refresh-error"></div>';
    $html .= '</div>';
    $form['refresh'] = array('#markup' => new FormattableMarkup($html, []));

    /* icon position */
    $form['positions'] = array(
      '#type' => 'item',
      '#title' => t('Fast icon position:'),
      '#description' => t('Choose position of the floating icon.'),
      '#required' => TRUE
    );

    $options = array(
      'left' => 'left (default - bottom left)',
      'right' => 'right (bottom right)',
      'topleft' => 'top left',
      'topright' => 'top right',
      'custom' => htmlentities('<custom ID of an element on page> : '),
      'none' => 'none'
    );

    $default_value = $config->get('fast_icon_position', 'left');

    foreach ($options as $key => $value) {
      if ($key == 'custom') {
        // special for custom - add a textfield
        $form['positions'][$key] = array(
          // The 'container-inline' class places elements next to each other, while the 'form-item' class provides the correct spacing between options.
          '#prefix' => '<div class="container-inline form-item">',
          '#suffix' => '</div>'
        );
        // By supplying the title here, instead of using the '#field_prefix' property of the textfield, clicking the text will also select the radio button.
        $form['positions'][$key]['custom_option'] = array(
          '#type' => 'radio',
          '#title' => $value,
          '#default_value' => $default_value,
          '#parents' => array('positions'),
          '#return_value' => $key,
          '#id' => $key
        );
        $form['positions'][$key]['custom_textfield'] = array(
          '#type' => 'textfield',
          '#default_value' => $config->get('fast_icon_position_custom_id', ''),
          '#size' => 20,
          '#attributes' => array('onClick' => 'javascript: selectCustom();'),
          '#suffix' => '</div>' // End of the "form-radios" style.
        );
      } else {
        $form['positions'][$key] = array(
          '#type' => 'radio',
          '#title' => $value,
          '#default_value' => $default_value,
          '#parents' => array('positions'),
          '#return_value' => $key,
          '#id' => $key
        );
        if ($key == 'left') {
          // The first radio button needs to make the style match a normal radios group.
          $form['positions'][$key]['#prefix'] = '<div class="form-radios">';
        }
      }

      /* successful login redirect */
      $form['user_redirect'] = array(
        '#type' => 'textfield',
        '#title' => t('Successful login redirect:'),
        '#default_value' => $config->get('fast_user_redirect'),
        '#description' => htmlentities(t('Choose where to redirect user after successful login (ie. <front> (for home page) / cart / checkout etc...).')),
        '#required' => TRUE,
        '#id' => 'fast-user-redirect'
      );

      /* action for authenticated users who are not registered on site */
      $options = array(
        'show_error' => t('Show error at login, saying "No user found"'),
        'create_account' => t('Create a new account with just email'),
        'create_and_register' => t('Create a new account with just email but take to registration page to complete profile')
      );
      $form['non_user_action'] = array(
        '#type' => 'radios',
        '#title' => t('Action for not registered users:'),
        '#options' => $options,
        '#default_value' => $config->get('fast_non_user_action'),
        '#description' => t('Choose what happens for authenticated users who are not registered on site.'),
        '#required' => TRUE,
        '#id' => 'fast-non-user-action'
      );
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
    * Validation handler for the config setings form.
 */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $position = $form_state->getValue('positions');
    if ($position == 'custom') {
      // get the value in textfield
      $position = $form_state->getValue('custom_textfield');
      if (empty($position)) {
        $form_state->setErrorByName('custom_textfield', t('Specify a valid ID of an element on page.'));
      }
    }

    $user_redirect = $form_state->getValue('user_redirect');
    if (empty($user_redirect)) {
      $form_state->setErrorByName('user_redirect', t('Specify a valid redirect URL.'));
    } else {
      $is_valid = \Drupal::service('path.validator')->isValid($user_redirect);
      if (!$is_valid) {
        // Not a system URL.
        try {
          $alias = \Drupal::service('path.alias_manager')->getAliasByPath($user_redirect);
          if (strcmp($alias, $user_redirect) == 0) {
            // Not a path alias.
            // $user_redirect does not exist.
            $form_state->setErrorByName('user_redirect', t('@path is not a valid, registered URL on the site.', array('@path' => $user_redirect)));
          }
        }
        catch (\InvalidArgumentException $e) {
          $form_state->setErrorByName('user_redirect', $e->getMessage());
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * Submit handler for the config setings form.
 */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('fast_login.settings');
    $config->set('fast_app_key', $form_state->getValue('app_key'));
    $config->set('fast_app_secret', $form_state->getValue('app_secret'));
    $position = $form_state->getValue('positions');
    if ($position == 'custom') {
      // get the value in textfield and store it too
      $config->set('fast_icon_position_custom_id', $form_state->getValue('custom_textfield'));
    }
    $config->set('fast_icon_position', $position);
    $config->set('fast_user_redirect', $form_state->getValue('user_redirect'));
    $config->set('fast_non_user_action', $form_state->getValue('non_user_action'));
    $config->save();

    // clear library cache - this is required so that the fast js external
    // library will be dynamically loaded with proper parameters key & icon which
    // may have been modified by user in the config settings page
    \Drupal::service('library.discovery')->clearCachedDefinitions();

    // clear the fast_login block cache - with the above, even though the js
    // library cache is cleared, it does not suffice for the block content to be
    // refreshed; setting max-age to 0 (which is done already) helps to rebuild
    // the block content, pages that use the block (especially for anonymous
    // user where content is read from cache) need to reload the block. So using
    // cache tag to clear block content specifically and be rebuilt and stitched
    // to page content
    \Drupal::service('cache_tags.invalidator')->invalidateTags(['fast_login.fast_login_block']);

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fast_login.settings',
    ];
  }
}
