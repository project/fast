<?php

namespace Drupal\fast_login\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;

/**
 * Provides a 'Fax' block.
 *
 * @Block(
 *   id = "fast_login_block",
 *   admin_label = @Translation("Fast Login block"),
 * )
 */
class FastLoginBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('fast_login.settings');
    $content  = '';
    $content .= '<div id="fast-login-holder" style="width:300px">';
    //$content .= '<fast-button></fast-button>';
    $content .= '</div>';

    $block = array(
      'type' => 'markup',
      '#markup' => $content,
      '#allowed_tags' => ['div', 'fast-button'],
      '#attached' => array(
        'library' =>  array(
          'fast_login/fast_js'
        ),
      ),
      '#cache' => array(
        'max-age' => 0
      )
     );

     //kint($block);
     return $block;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  public function getCacheMaxAge() {
    return 0;
  }

  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['fast_login.fast_login_block']);
  }
}
