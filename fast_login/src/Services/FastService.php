<?php

namespace Drupal\fast_login\Services;

use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

class FastService {
  const FAST_BASE_URI = 'https://api.fast.co/api/';
  private $client;

  public function __construct() {

  }

  /**
   * Implements the enroll Fast Platform API
   *
   * Uses http_client_factory and GuzzleHttp to invoke the HTTP GET call on Fast Server
   * Upon return uses Json::decode to parse the response to check for
   * value in 'status' field and sets 'error' of the result array if it is not 200.
   * Also any error is passed on in 'error' of the result array.
   *
   * @return
   *   an array (decoded JSON response from Fast server); will contain an element
   *            'error', if any error encountered.
   */
  public function enroll() {
    global $base_url;
    global $base_path;

    $config = \Drupal::config('fast_login.settings');
    $system_site_config = \Drupal::config('system.site');

    $result = array();

    $data = array('platform' => 'Drupal',
      // global PHP variable in includes/bootstrap.inc within D7
      'version' => \DRUPAL::VERSION,
      // site wide email of admin who will own the app
      'identifier' => $system_site_config->get('mail'),
      // redirect URL which will verify login attempt after successful authentication through Fast
      'redirect' => $base_url . $base_path . 'fast-login/authenticate'
    );

    $error_msg = t('Failed to enroll with Fast Platform API Server');
    $full_url = Url::fromUri(self::FAST_BASE_URI . 'enroll');
    $clientFactory = \Drupal::service('http_client_factory');
    $client = $clientFactory->fromOptions(['verify' => FALSE]);
    $response = NULL;
    try {
      $response = $client->request(
        'GET',
        $full_url->toString(),
        [
          'query' => $data,
          'verify' => false,
          'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded'
          ]
        ]
      );
      $response = $response->getBody()->getContents();
    }
    catch (ClientException $e) {
      $error_msg .= ' - ' . $e->GetMessage;
      drupal_set_message($error_msg, 'error');
      \Drupal::logger('fast_login')->error($error_msg);
      $result['error'] = $error_msg;
      return $result;
    }

    $result = Json::decode($response);
    if (isset($result['status']) && $result['status'] != 200) {
      // no http failure - but enroll was not successful
      $error_msg .= ' - ' . t('returned status value is: ') . $result['status'];
      \Drupal::logger('fast_login')->error($error_msg);
      $result['error'] = $error_msg;
    }

    return $result;
  }

  /**
   * Implements the verify Fast Platform API
   *
   * Uses http_client_factory and GuzzleHttp to invoke the HTTP GET call on Fast Server
   * Upon return uses Json::decode to parse the response to check for
   * value in 'success' field and sets 'error' of the result array if it is false.
   * Also any error is passed on in 'error' of the result array.
   *
   * @param $data
   *   an array that contains additional GET vars to be passed to the verify API call.
   *   The caller should ensure required elements such as 'challengeId', 'oth',
   *   'identifier' are set for proper authentication at the Fast server.
   * @return
   *   an array (decoded JSON response from Fast server); will contain an element
   *            'error', if any error encountered.
   */
  function verify($data = array()) {
    $config = \Drupal::config('fast_login.settings');
    $system_site_config = \Drupal::config('system.site');

    $result = array();
    $error_msg = t('Failed to verify login attempt with Fast Platform API Server');
    $full_url = Url::fromUri(self::FAST_BASE_URI . 'verify');
    $clientFactory = \Drupal::service('http_client_factory');
    $client = $clientFactory->fromOptions(['verify' => FALSE]);
    $response = NULL;
    try {
      $response = $client->request(
        'GET',
        $full_url->toString(),
        [
          'query' => $data,
          'verify' => false,
          'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded'
          ],
        ]
      );
      $response = $response->getBody()->getContents();
    }
    catch (Exception $e) {
      print_r($e->getMessage());
      $error_msg .= ' - ' . $e->getMessage();
      drupal_set_message($error_msg, 'error');
      \Drupal::logger('fast_login')->error($error_msg);
      $result['error'] = $error_msg;
      return $result;
    }

    $result = Json::decode($response);
    if (empty($result['success'])) {
      // no http failure - but verify was not successful
      $error_msg .= ' - ' . t('Login verification was not successful at Fast server.');
      \Drupal::logger('fast_login')->error($error_msg);
      $result['error'] = $error_msg;
    }

    return $result;
  }

}
